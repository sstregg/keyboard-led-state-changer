﻿using System;
using System.Runtime.InteropServices;

namespace KeyboardLedState.Helpers
{
    /// <summary>
    /// Кэш для повторного переиспользования ранее выделенной памяти под структуру SP_DEVICE_INTERFACE_DETAIL_DATA.
    /// Также позволяет получить данные из структуры.
    /// </summary>
    class DeviceInterfaceDetailDataLocalCache : IDisposable
    {
        private static readonly int MinimumBufferSize = IntPtr.Size == 4 ? 6 : 8;

        private IntPtr mBufferPtr;
        private int mBufferCapacity;
        private int mBufferLength;

        public IntPtr Alloc ( int size )
        {
            if ( size < MinimumBufferSize )
                throw new ArgumentException( "", "size" );

            if ( mBufferPtr == IntPtr.Zero )
            {
                mBufferCapacity = size;
                mBufferPtr = Marshal.AllocHGlobal( size );
            }
            else if ( mBufferCapacity < size )
            {
                mBufferCapacity = size;
                mBufferPtr = Marshal.ReAllocHGlobal( mBufferPtr, (IntPtr)size );
            }

            mBufferLength = size;
            PrepareBuffer();

            return mBufferPtr;
        }

        private void PrepareBuffer ( )
        {
            Interop.RtlZeroMemory( mBufferPtr, (IntPtr)mBufferCapacity );
            Marshal.WriteInt32( mBufferPtr, MinimumBufferSize );
        }

        public int Capacity
        {
            get
            {
                if ( mBufferPtr == IntPtr.Zero )
                    throw new Exception();

                return mBufferCapacity;
            }
        }

        public int Length
        {
            get
            {
                if ( mBufferPtr == IntPtr.Zero )
                    throw new Exception();

                return mBufferLength;
            }
        }

        public int DataSize
        {
            get
            {
                int roundedSize = Length & ~0x1;

                return (roundedSize - 4) / sizeof( char ) - 1;
            }
        }

        public string GetData ( )
        {
            if ( mBufferPtr == IntPtr.Zero )
                throw new Exception();

            return Marshal.PtrToStringUni( mBufferPtr + 4, DataSize );
        }

        public void Dispose ( )
        {
            if ( mBufferPtr != IntPtr.Zero )
            {
                Marshal.FreeHGlobal( mBufferPtr );

                mBufferPtr = IntPtr.Zero;
                mBufferCapacity = 0;
                mBufferLength = 0;
            }
        }
    }
}