﻿using System;

internal static partial class Interop
{
    internal struct KEYBOARD_INDICATOR_PARAMETERS
    {
        /// <summary>
        /// Unit identifier.  Specifies the device unit for which this
        /// request is intended.
        /// </summary>
        internal ushort UnitId;
        /// <summary>
        /// LED indicator state.
        /// </summary>
        internal ushort LedFlags;
    }

    internal struct SP_DEVICE_INTERFACE_DATA
    {
        internal int cbSize;
        internal Guid  InterfaceClassGuid;
        internal uint Flags;
        internal UIntPtr Reserved;
    }

    internal struct SP_DEVICE_INTERFACE_DETAIL_DATA
    {
        //internal int cbSize;
        //internal fixed char DevicePath[1];
    }
}