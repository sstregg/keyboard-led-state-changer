﻿using System;
using System.Runtime.InteropServices;

internal static partial class Interop
{
    #region ~ SetupAPI ~

    private const string SetupApiLib = "setupapi.dll";

    [DllImport( SetupApiLib, SetLastError = true, CharSet = ApiStringEncoding )]
    internal static extern IntPtr SetupDiGetClassDevs (
       ref Guid ClassGuid,
       string Enumerator,
       IntPtr hwndParent,
       uint Flags
       );

    [DllImport( SetupApiLib, SetLastError = true )]
    [return: MarshalAs( UnmanagedType.Bool )]
    internal static extern bool SetupDiEnumDeviceInterfaces (
        IntPtr DeviceInfoSet,
        IntPtr DeviceInfoData,
        ref Guid InterfaceClassGuid,
        uint MemberIndex,
        out SP_DEVICE_INTERFACE_DATA DeviceInterfaceData
        );

    [DllImport( SetupApiLib, SetLastError = true, CharSet = ApiStringEncoding )]
    [return: MarshalAs( UnmanagedType.Bool )]
    internal static extern bool SetupDiGetDeviceInterfaceDetail (
        IntPtr DeviceInfoSet,
        ref SP_DEVICE_INTERFACE_DATA DeviceInterfaceData,
        IntPtr DeviceInterfaceDetailData,
        int DeviceInterfaceDetailDataSize,
        out int RequiredSize,
        IntPtr DeviceInfoData
        );

    [DllImport( SetupApiLib, SetLastError = true )]
    [return: MarshalAs( UnmanagedType.Bool )]
    internal static extern bool SetupDiDestroyDeviceInfoList (
        IntPtr DeviceInfoSet
        );

    #endregion // SetupAPI

    #region ~ Kernel32 ~

    private const string Kernel32Lib = "kernel32.dll";

    [DllImport( Kernel32Lib, SetLastError = true, CharSet = ApiStringEncoding )]
    internal static extern IntPtr CreateFile (
        string lpFileName,
        uint dwDesiredAccess,
        uint dwShareMode,
        IntPtr lpSecurityAttributes,
        uint dwCreationDisposition,
        uint dwFlagsAndAttributes,
        IntPtr hTemplateFile
        );

    [DllImport( Kernel32Lib, SetLastError = true )]
    [return: MarshalAs( UnmanagedType.Bool )]
    internal static extern bool CloseHandle (
        IntPtr Handle
        );

    [DllImport( Kernel32Lib, SetLastError = true, EntryPoint = "DeviceIoControl" )]
    [return: MarshalAs( UnmanagedType.Bool )]
    internal static extern bool DeviceIoControlIn (
        IntPtr hDevice,
        uint dwIoControlCode,
        ref KEYBOARD_INDICATOR_PARAMETERS lpInBuffer,
        int nInBufferSize,
        IntPtr lpOutBuffer,
        int nOutBufferSize,
        out int lpBytesReturned,
        IntPtr lpOverlapped
        );

    [DllImport( Kernel32Lib, SetLastError = true, EntryPoint = "DeviceIoControl" )]
    [return: MarshalAs( UnmanagedType.Bool )]
    internal static extern bool DeviceIoControlOut (
        IntPtr hDevice,
        uint dwIoControlCode,
        IntPtr lpInBuffer,
        int nInBufferSize,
        out KEYBOARD_INDICATOR_PARAMETERS lpOutBuffer,
        int nOutBufferSize,
        out int lpBytesReturned,
        IntPtr lpOverlapped
        );

    #endregion // Kernel32

    #region ~ ntdll ~

    private const string NtdllLib = "ntdll.dll";

    [DllImport( NtdllLib )]
    internal static extern void RtlZeroMemory (
        IntPtr Destination,
        IntPtr Length
        );

    #endregion // ntdll
}