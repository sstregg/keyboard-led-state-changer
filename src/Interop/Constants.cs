﻿using System;
using System.Runtime.InteropServices;

internal static partial class Interop
{
    internal readonly static IntPtr INVALID_HANDLE_VALUE = (IntPtr)(-1);

    internal readonly static Guid GUID_DEVINTERFACE_KEYBOARD = new Guid(
        0x884b96c3, 0x56ef, 0x11d1, 0xbc, 0x8c, 0x00, 0xa0, 0xc9, 0x14, 0x05, 0xdd
        );
    internal const uint IOCTL_KEYBOARD_SET_INDICATORS = 0x000b0008;
    internal const uint IOCTL_KEYBOARD_QUERY_INDICATORS = 0x000b0040;

    internal const uint FILE_SHARE_READ = 0x00000001;
    internal const uint FILE_SHARE_WRITE = 0x00000002;
    internal const uint FILE_SHARE_RW = FILE_SHARE_WRITE | FILE_SHARE_READ;
    internal const uint OPEN_EXISTING = 3;

    internal const uint DIGCF_PRESENT = 0x00000002;
    internal const uint DIGCF_DEVICEINTERFACE = 0x00000010;

    internal const int ERROR_INSUFFICIENT_BUFFER = 122;
    internal const int ERROR_NO_MORE_ITEMS = 259;

    #region ~ API CharSet ~

    private const CharSet ApiStringEncoding = CharSet.Unicode;

    #endregion
}