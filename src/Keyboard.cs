﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace KeyboardLedState
{
    public class Keyboard : IDisposable
    {
        private IntPtr mKeyboardHandle;

        internal Keyboard ( string devicePath )
        {
            if ( string.IsNullOrEmpty( devicePath ) )
                throw new ArgumentNullException( "devicePath" );

            DevicePath = devicePath;
        }

        public Keyboard Open ( )
        {
            if ( mKeyboardHandle != IntPtr.Zero )
                throw new Exception();

            IntPtr hKeyboard = Interop.CreateFile(
                        DevicePath, 0,
                        Interop.FILE_SHARE_RW,
                        IntPtr.Zero,
                        Interop.OPEN_EXISTING,
                        0, IntPtr.Zero
                        );

            if ( hKeyboard == Interop.INVALID_HANDLE_VALUE )
                throw new Win32Exception();

            mKeyboardHandle = hKeyboard;

            return this;
        }

        public string DevicePath { get; private set; }

        public KeyboardLed LedState
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                if ( mKeyboardHandle == IntPtr.Zero )
                    throw new Exception();

                var kip = new Interop.KEYBOARD_INDICATOR_PARAMETERS {
                    LedFlags = (ushort)value
                };

                int zero;
                bool result =  Interop.DeviceIoControlIn(
                                    mKeyboardHandle, Interop.IOCTL_KEYBOARD_SET_INDICATORS,
                                    ref kip, Marshal.SizeOf( kip ), IntPtr.Zero, 0,
                                    out zero, IntPtr.Zero
                                    );

                if ( !result )
                    throw new Win32Exception();
            }
        }

        public void Close ( )
        {
            Dispose();
        }

        public void Dispose ( )
        {
            if ( mKeyboardHandle != IntPtr.Zero )
            {
                if ( !Interop.CloseHandle( mKeyboardHandle ) )
                    throw new Win32Exception();
                mKeyboardHandle = IntPtr.Zero;
            }
        }
    }
}