﻿using System;

namespace KeyboardLedState
{
    [Flags]
    public enum KeyboardLed : ushort
    {
        None = 0,
        ScrollLock = 1,
        NumLock = 2,
        CapsLock = 4
    }
}