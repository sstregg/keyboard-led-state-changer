﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;
using KeyboardLedState.Helpers;

namespace KeyboardLedState
{
    public class KeyboardsEnumerator : IEnumerable<Keyboard>, IEnumerator<Keyboard>
    {
        private Guid mKeyboardInterfaceGuid;
        private IntPtr mKeyboardDevsClassHandle;
        private uint? mDevIndex;
        private Keyboard mCurrent;
        private Interop.SP_DEVICE_INTERFACE_DATA mDevInterfaceData;
        private DeviceInterfaceDetailDataLocalCache mDataCache;

        public KeyboardsEnumerator ( )
        {
            Initialize();
            OpenDeviceInformationSet();
        }

        private void Initialize ( )
        {
            mKeyboardInterfaceGuid = Interop.GUID_DEVINTERFACE_KEYBOARD;
            mDevInterfaceData.cbSize = Marshal.SizeOf( mDevInterfaceData );
            mDataCache = new DeviceInterfaceDetailDataLocalCache();
        }

        private void OpenDeviceInformationSet ( )
        {
            IntPtr hDevsClass = Interop.SetupDiGetClassDevs(
                                    ref mKeyboardInterfaceGuid,
                                    null, IntPtr.Zero,
                                    Interop.DIGCF_PRESENT | Interop.DIGCF_DEVICEINTERFACE
                                    );

            if ( hDevsClass == Interop.INVALID_HANDLE_VALUE )
                throw new Win32Exception();

            mKeyboardDevsClassHandle = hDevsClass;
        }

        public Keyboard Current
        {
            get
            {
                if ( null == mDevIndex )
                    return null;

                return mCurrent;
            }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        public bool MoveNext ( )
        {
            if ( null == mDevIndex )
                mDevIndex = 0;

            // Перечисление интерфейсов
            bool result = Interop.SetupDiEnumDeviceInterfaces(
                            mKeyboardDevsClassHandle, IntPtr.Zero,
                            ref mKeyboardInterfaceGuid, mDevIndex.Value,
                            out mDevInterfaceData
                            );

            if ( !result )
            {
                if ( Marshal.GetLastWin32Error() != Interop.ERROR_NO_MORE_ITEMS )
                    throw new Win32Exception();

                return false;
            }

            // Узнаём размер необходимого буфера
            int buffSize;
            result = Interop.SetupDiGetDeviceInterfaceDetail(
                        mKeyboardDevsClassHandle, ref mDevInterfaceData,
                        IntPtr.Zero, 0, out buffSize, IntPtr.Zero
                        );

            if ( !result && Marshal.GetLastWin32Error() != Interop.ERROR_INSUFFICIENT_BUFFER )
                throw new Win32Exception();

            IntPtr pMem = mDataCache.Alloc( buffSize );
            result = Interop.SetupDiGetDeviceInterfaceDetail(
                            mKeyboardDevsClassHandle, ref mDevInterfaceData,
                            pMem, buffSize, out buffSize, IntPtr.Zero
                            );

            if ( !result )
                throw new Win32Exception();

            mCurrent = new Keyboard( mDataCache.GetData() );

            mDevIndex++;
            return true;
        }

        public void Reset ( )
        {
            mDevIndex = null;
        }

        public void Dispose ( )
        {
            if ( mKeyboardDevsClassHandle != IntPtr.Zero )
            {
                if ( !Interop.SetupDiDestroyDeviceInfoList( mKeyboardDevsClassHandle ) )
                    throw new Win32Exception();

                mKeyboardDevsClassHandle = IntPtr.Zero;
                mDataCache.Dispose();
                mDataCache = null;
            }
        }

        public IEnumerator<Keyboard> GetEnumerator ( )
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator ( )
        {
            return GetEnumerator();
        }
    }

}